﻿namespace Exercise3_distance_converter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numberToConvertTxtBox = new System.Windows.Forms.TextBox();
            this.convertFeetToYardsBtn = new System.Windows.Forms.Button();
            this.numberConvertedTxtBox = new System.Windows.Forms.TextBox();
            this.EnterDistanceLbl = new System.Windows.Forms.Label();
            this.DistanceConvertedLbl = new System.Windows.Forms.Label();
            this.convertFeetToMetersBtn = new System.Windows.Forms.Button();
            this.convertYardsToMetersBtn = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // numberToConvertTxtBox
            // 
            this.numberToConvertTxtBox.Location = new System.Drawing.Point(389, 43);
            this.numberToConvertTxtBox.MinimumSize = new System.Drawing.Size(200, 50);
            this.numberToConvertTxtBox.Name = "numberToConvertTxtBox";
            this.numberToConvertTxtBox.Size = new System.Drawing.Size(200, 26);
            this.numberToConvertTxtBox.TabIndex = 0;
            // 
            // convertFeetToYardsBtn
            // 
            this.convertFeetToYardsBtn.Location = new System.Drawing.Point(89, 191);
            this.convertFeetToYardsBtn.MinimumSize = new System.Drawing.Size(500, 50);
            this.convertFeetToYardsBtn.Name = "convertFeetToYardsBtn";
            this.convertFeetToYardsBtn.Size = new System.Drawing.Size(500, 50);
            this.convertFeetToYardsBtn.TabIndex = 1;
            this.convertFeetToYardsBtn.Text = "Convert Feet to Yards";
            this.convertFeetToYardsBtn.UseVisualStyleBackColor = true;
            this.convertFeetToYardsBtn.Click += new System.EventHandler(this.Button1_Click);
            // 
            // numberConvertedTxtBox
            // 
            this.numberConvertedTxtBox.Location = new System.Drawing.Point(389, 112);
            this.numberConvertedTxtBox.MinimumSize = new System.Drawing.Size(200, 50);
            this.numberConvertedTxtBox.Name = "numberConvertedTxtBox";
            this.numberConvertedTxtBox.Size = new System.Drawing.Size(200, 26);
            this.numberConvertedTxtBox.TabIndex = 2;
            // 
            // EnterDistanceLbl
            // 
            this.EnterDistanceLbl.AutoSize = true;
            this.EnterDistanceLbl.Location = new System.Drawing.Point(85, 43);
            this.EnterDistanceLbl.MinimumSize = new System.Drawing.Size(200, 50);
            this.EnterDistanceLbl.Name = "EnterDistanceLbl";
            this.EnterDistanceLbl.Size = new System.Drawing.Size(200, 50);
            this.EnterDistanceLbl.TabIndex = 3;
            this.EnterDistanceLbl.Text = "Please enter a distance:";
            // 
            // DistanceConvertedLbl
            // 
            this.DistanceConvertedLbl.AutoSize = true;
            this.DistanceConvertedLbl.Location = new System.Drawing.Point(85, 112);
            this.DistanceConvertedLbl.MinimumSize = new System.Drawing.Size(200, 50);
            this.DistanceConvertedLbl.Name = "DistanceConvertedLbl";
            this.DistanceConvertedLbl.Size = new System.Drawing.Size(200, 50);
            this.DistanceConvertedLbl.TabIndex = 4;
            this.DistanceConvertedLbl.Text = "The distance converted:";
            // 
            // convertFeetToMetersBtn
            // 
            this.convertFeetToMetersBtn.Location = new System.Drawing.Point(89, 247);
            this.convertFeetToMetersBtn.MinimumSize = new System.Drawing.Size(500, 50);
            this.convertFeetToMetersBtn.Name = "convertFeetToMetersBtn";
            this.convertFeetToMetersBtn.Size = new System.Drawing.Size(500, 50);
            this.convertFeetToMetersBtn.TabIndex = 5;
            this.convertFeetToMetersBtn.Text = "Convert Feet to Meters";
            this.convertFeetToMetersBtn.UseVisualStyleBackColor = true;
            this.convertFeetToMetersBtn.Click += new System.EventHandler(this.Button2_Click);
            // 
            // convertYardsToMetersBtn
            // 
            this.convertYardsToMetersBtn.Location = new System.Drawing.Point(89, 303);
            this.convertYardsToMetersBtn.MinimumSize = new System.Drawing.Size(500, 50);
            this.convertYardsToMetersBtn.Name = "convertYardsToMetersBtn";
            this.convertYardsToMetersBtn.Size = new System.Drawing.Size(500, 50);
            this.convertYardsToMetersBtn.TabIndex = 6;
            this.convertYardsToMetersBtn.Text = "Convert Yards to Meters";
            this.convertYardsToMetersBtn.UseVisualStyleBackColor = true;
            this.convertYardsToMetersBtn.Click += new System.EventHandler(this.Button3_Click);
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(89, 359);
            this.clearBtn.MinimumSize = new System.Drawing.Size(500, 50);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(500, 50);
            this.clearBtn.TabIndex = 7;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.Button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.convertYardsToMetersBtn);
            this.Controls.Add(this.convertFeetToMetersBtn);
            this.Controls.Add(this.DistanceConvertedLbl);
            this.Controls.Add(this.EnterDistanceLbl);
            this.Controls.Add(this.numberConvertedTxtBox);
            this.Controls.Add(this.convertFeetToYardsBtn);
            this.Controls.Add(this.numberToConvertTxtBox);
            this.Name = "Form1";
            this.Text = "Distance Convertor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox numberToConvertTxtBox;
        private System.Windows.Forms.Button convertFeetToYardsBtn;
        private System.Windows.Forms.TextBox numberConvertedTxtBox;
        private System.Windows.Forms.Label EnterDistanceLbl;
        private System.Windows.Forms.Label DistanceConvertedLbl;
        private System.Windows.Forms.Button convertFeetToMetersBtn;
        private System.Windows.Forms.Button convertYardsToMetersBtn;
        private System.Windows.Forms.Button clearBtn;
    }
}

